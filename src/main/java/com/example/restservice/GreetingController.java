package com.example.restservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

//    private Greetings myId;
//    private Greetings myContent;

//    public GreetingController(Greetings theId,Greetings theContent){
//        this.myId=theId;
//        this.myContent=theContent;
//    }

    private static final String template="Hello, %s!";
    private final AtomicLong counter=new AtomicLong();



    @GetMapping("/greetings")
    public Greetings greet(
            @RequestParam(value="name",defaultValue = "World")String name){
        return new Greetings(counter.incrementAndGet(),String.format(template,name));//name=%s
    }
}
